const findLastCar = require('../problem2');
const inventory = require('../inventory.json');
const car=findLastCar(inventory);
if(car){
    console.log(`Last car is a ${car.car_make} ${car.car_model}`);
}else{
    console.log(`No cars found in the inventory`);
}

