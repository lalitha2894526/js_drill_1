function getCarYears(inventory) {
    const years = [];
    try {
        for (let index = 0; index < inventory.length; index++) {
            years.push(inventory[index].car_year);
        }
    } catch (error) {
        console.error('An error occurred:', error.message);
        return [];
    }
    return years;
}

module.exports = getCarYears;
