function findLastCar(inventory) {
    try {
        if (!Array.isArray(inventory) || inventory.length === 0) {
            throw new Error('Inventory must be a non-empty array');
        }
        return inventory[inventory.length - 1];
    } 
    catch (error) {
        console.error('Error occurred:', error.message);
        return null;
    }
}

module.exports = findLastCar;
