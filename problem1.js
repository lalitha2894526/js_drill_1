function findCarById(inventory, id) {
    try {
        if (!Array.isArray(inventory)) {
            throw new Error('Inventory must be an array');
        }
        if (typeof id !== 'number') {
            throw new Error('ID must be a number');
        }

        for (let index = 0; index < inventory.length; index++) {
            if (inventory[index].id === id) {
                return inventory[index];
            }
        }
        return null;
    } 
    catch (error) {
        console.error('Error occurred:', error.message);
        return null;
    }
}
module.exports = findCarById;
