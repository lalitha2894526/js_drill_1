function getOlderCars(inventory) {
    const olderCars = [];
    try {
        for (let index = 0; index < inventory.length; index++) {
            if (inventory[index].car_year < 2000) {
                olderCars.push(inventory[index]);
            }
        }
    } catch (error) {
        console.error('An error occurred:', error.message);
        return [];
    }
    return olderCars;
}

module.exports = getOlderCars;
