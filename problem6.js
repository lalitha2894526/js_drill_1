function filterBMWAndAudi(inventory) {
    const BMWAndAudi = [];
    try {
        for (let index = 0; index < inventory.length; index++) {
            if (inventory[index].car_make === 'BMW' || inventory[index].car_make === 'Audi') {
                BMWAndAudi.push(inventory[index]);
            }
        }
    } 
    catch (error) {
        console.error('An error occurred:', error.message);
        return [];
    }
    return BMWAndAudi;
}

module.exports = filterBMWAndAudi;
