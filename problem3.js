 function sortCarModels(inventory) {
    if (!Array.isArray(inventory) || inventory.length === 0) {
        throw new Error('Invalid inventory data');
    }
    let carModels = [];
    for (let index1 = 0; index1 < inventory.length; index1++) {
        if (typeof inventory[index1].car_model !== 'string') {
            throw new Error('Car models must be strings');
        }
        carModels.push(inventory[index1].car_model);
    }
    let n = carModels.length;
    for (let index1 = 0; index1 < n - 1; index1++) {
        for (let index2 = 0; index2 < n - index1 - 1; index2++) {
            if (carModels[index2] > carModels[index2 + 1]) {
                let temp = carModels[index2];
                carModels[index2] = carModels[index2 + 1];
                carModels[index2 + 1] = temp;
            }
        }
    }
    return carModels;
}

module.exports = sortCarModels;
